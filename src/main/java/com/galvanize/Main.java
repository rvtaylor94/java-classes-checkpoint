package com.galvanize;

public class Main {
    public static void main(String[] args) {
        CallingCard card1 = new CallingCard(20);
        card1.addDollars(1);

        CellPhone phone = new CellPhone(card1);

        phone.call("747-4175");
        System.out.println(phone.getHistory());
        phone.tick();
        phone.tick();
        phone.tick();
        phone.tick();
        phone.tick();
        phone.tick();
        System.out.println(phone.isTalking());
        phone.endCall();
        System.out.println(phone.isTalking());
        card1.addDollars(5);
        phone.call("636-7439");
        phone.tick();
        phone.tick();
        phone.endCall();
        phone.call("911");
        phone.tick();
        phone.endCall();
        System.out.println(phone.getHistory());
        System.out.println(card1.getRemainingMinutes());

    }
}
