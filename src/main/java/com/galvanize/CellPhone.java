
package com.galvanize;

import java.util.ArrayList;

public class CellPhone {
    private CallingCard card;
    private boolean isTalking = false;
    private boolean hasCutOff = false;
    private String number;
    private String history = "";
    private int minutes;
    public CellPhone(CallingCard card) {
        this.card = card;
    }
    public void call(String number) {
        if (isTalking) {
            System.out.println("Already on the phone.");
        } else {
            isTalking = true;
            this.number = number;
        }
    }
    public boolean isTalking() {
        return isTalking;
    }
    public void tick() {
        card.useMinutes(1);
        minutes++;
        if (card.getRemainingMinutes() == 0) {
            hasCutOff = true;
            endCall();
        }
    }
    public void endCall() {
        if (!isTalking) {
            System.out.println("Call has already ended.");
            return;
        }

        if (history.length() > 0) {
            history += ", ";
        }
        history += (String.format("%s %s%s %s)", number, hasCutOff ? "(cut off at " : "(", Integer.toString(minutes), minutes > 1 ? "minutes" : "minute"));

        isTalking = false;
        number = "";
        minutes = 0;
        hasCutOff = false;
    }
    public String getHistory() {
        return history;
    }
}
