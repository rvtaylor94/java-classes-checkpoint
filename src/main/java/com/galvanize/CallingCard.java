
package com.galvanize;

public class CallingCard {
    private int centsPerMinute;
    private int remainingMinutes;

    public CallingCard(int centsPerMinute) {
        this.centsPerMinute = centsPerMinute;
    }

    public void addDollars(int dollars) {
        remainingMinutes += (dollars * 100) / centsPerMinute;
    }
    public int getRemainingMinutes() {
        if (remainingMinutes < 0) {
            remainingMinutes = 0;
        }
        return remainingMinutes;
    }
    public void useMinutes(int minutes) {
        remainingMinutes -= minutes;
    }
}
